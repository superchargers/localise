<?php

namespace ArkSys\Localise;

/** You need to have php-curl extension installed for this library to work. **
*
*##  Api version : 1 ##
*
*  If you want to use Localise in test mode, you have to set your $_ENV['LOCALISE_TEST'] variable to true.
*  Your webhook will be called automatically after 3 seconds after your translation call, webhook request will contain your file.
*
*##   Localise class - example of calling methods: ##
*
*  use ArkSys\Localise\Localise;
*
*  $token = (Localise::getToken($userName, $password))->token;
*
*  $localise = new Localise($token);
*
*  Above code should be used for testing purpose only. In production programmers should persist returned
*  token and use it in other calls.
*
*  use ArkSys\Localise\Localise;
*
*  get previously generated token ($originalToken) from database or file or redis, ...
*
*  $token = Localise::getToken($userName, $password, $originalToken);
*
*  if($token->token != $originalToken->token)
*  {
*    ... //persist newly generated token ($token)
*  }
*
*  $localise = new Localise($token);
*  $localise->withLanguageFrom($languageFrom) // string - should match const LANGUAGE_... of this class
*           ->withLanguagesTo($languagesTo) // string|array of strings - should match const LANGUAGE_... of this class
*           ->withReference($reference) // optional, your own reference for requested translation - max 255 characters
*           ->withJobDue($jobDue) //optional - DateTime
*           ->withPreTranslate() // optional - experimental sends machine translated texts before human translations take place
*           ->translateText($text, $type = null); // if you want to send text to translation
*           OR
*           ->translateFile($filePath); // if you want to send file for translation
*
*  returns taskId and reference of your translation task and job ID for each of target languages as in
*  following example
*
*
*  [
*    'taskId' => 123,
*    'reference' => 'MY_OWN_REFERENCE',
*    'jobs' => [
*      'en' => 1234,
*      'de' => 1235
*    ]
*  ];
*
*##   *** Example of downloading translated file *** ##
*
*##   Downloading using jobId ###
*
*  $localise->downloadJob($jobId, $path); // $jobId - Id of translated job, $path - optional - path were translated file will be saved
*
*##   Downloading using reference and target language ###
*
*  $localise->downloadJobByReference($reference, $targetLanguage, $path) // $reference - your task reference, $targetLanguage - target language of job to be downloaded, $path - optional - path were translated file will be saved
*
*  Every download call returns json with taskId of downloaded file, filename and file content inside of file property. If you use **downloadJobByReference** method and in case you use multiple identical references **only the most recently completed translation will be returned**.
*
*##   *** Example of creating webhook *** ##
*
*  $localise->createWebhook($webhookName, $webhookUrl, $type);
*
*  $webhookName - name of webhook
*  $webhookUrl - url with will be called
*  $type - type of event - should match const WEBHOOK_TYPE_... of this class**
 *
*  You will be notified via POST request to your webhook URL when status of your task was changed.
*
*  Webhook request contains:
*  taskId, reference, sourceLanguage, targetLanguage, jobId and status
*/

use DateTime;

class Localise
{
    const LANGUAGE_ABKHAZIAN = 'ab';
    const LANGUAGE_AFAR = 'aa';
    const LANGUAGE_AFRIKAANS = 'af';
    const LANGUAGE_AKAN = 'ak';
    const LANGUAGE_ALBANIAN = 'sq';
    const LANGUAGE_AMHARIC = 'am';
    const LANGUAGE_ARABIC = 'ar';
    const LANGUAGE_ARAGONESE = 'an';
    const LANGUAGE_ARMENIAN = 'hy';
    const LANGUAGE_ASSAMESE = 'as';
    const LANGUAGE_AVARIC = 'av';
    const LANGUAGE_AVESTAN = 'ae';
    const LANGUAGE_AYMARA = 'ay';
    const LANGUAGE_AZERBAIJANI = 'az';
    const LANGUAGE_BAMBARA = 'bm';
    const LANGUAGE_BASHKIR = 'ba';
    const LANGUAGE_BASQUE = 'eu';
    const LANGUAGE_BELARUSIAN = 'be';
    const LANGUAGE_BENGALI = 'bn';
    const LANGUAGE_BIHARI = 'bh';
    const LANGUAGE_BISLAMA = 'bi';
    const LANGUAGE_BOSNIAN = 'bs';
    const LANGUAGE_BRETON = 'br';
    const LANGUAGE_BULGARIAN = 'bg';
    const LANGUAGE_BURMESE = 'my';
    const LANGUAGE_CATALAN_VALENCIAN = 'ca';
    const LANGUAGE_CENTRAL_KHMER = 'km';
    const LANGUAGE_CHAMORRO = 'ch';
    const LANGUAGE_CHECHEN = 'ce';
    const LANGUAGE_CHINESE = 'zh';
    const LANGUAGE_CHUVASH = 'cv';
    const LANGUAGE_CORNISH = 'kw';
    const LANGUAGE_CORSICAN = 'co';
    const LANGUAGE_CREE = 'cr';
    const LANGUAGE_CROATIAN = 'hr';
    const LANGUAGE_CZECH = 'cs';
    const LANGUAGE_DANISH = 'da';
    const LANGUAGE_DUTCH_FLEMISH = 'nl';
    const LANGUAGE_DZONGKHA = 'dz';
    const LANGUAGE_ENGLISH = 'en';
    const LANGUAGE_ENGLISH_UK = 'en-UK';
    const LANGUAGE_ENGLISH_US = 'en-US';
    const LANGUAGE_ESPERANTO = 'eo';
    const LANGUAGE_ESTONIAN = 'et';
    const LANGUAGE_EWE = 'ee';
    const LANGUAGE_FAROESE = 'fo';
    const LANGUAGE_FIJIAN = 'fj';
    const LANGUAGE_FILIPINO = 'fil';
    const LANGUAGE_FINNISH = 'fi';
    const LANGUAGE_FLEMISH = 'fl';
    const LANGUAGE_FRENCH = 'fr';
    const LANGUAGE_FULAH = 'ff';
    const LANGUAGE_GAELIC_SCOTTISH_GAELIC = 'gd';
    const LANGUAGE_GALICIAN = 'gl';
    const LANGUAGE_GANDA = 'lg';
    const LANGUAGE_GEORGIAN = 'ka';
    const LANGUAGE_GERMAN = 'de';
    const LANGUAGE_GERMAN_AUSTRIA = 'de-AT';
    const LANGUAGE_GERMAN_SWITZERLAND = 'de-CH';
    const LANGUAGE_GREEK_MODERN = 'el';
    const LANGUAGE_GUARANI = 'gn';
    const LANGUAGE_GUJARATI = 'gu';
    const LANGUAGE_HAITIAN = 'ht';
    const LANGUAGE_HAUSA = 'ha';
    const LANGUAGE_HEBREW = 'he';
    const LANGUAGE_HERERO = 'hz';
    const LANGUAGE_HINDI = 'hi';
    const LANGUAGE_HIRI_MOTU = 'ho';
    const LANGUAGE_HUNGARIAN = 'hu';
    const LANGUAGE_ICELANDIC = 'is';
    const LANGUAGE_IDO = 'io';
    const LANGUAGE_IGBO = 'ig';
    const LANGUAGE_INDONESIAN = 'id';
    const LANGUAGE_INTERLINGUA = 'ia';
    const LANGUAGE_INTERLINGUE_OCCIDENTAL = 'ie';
    const LANGUAGE_INUKTITUT = 'iu';
    const LANGUAGE_INUPIAQ = 'ik';
    const LANGUAGE_IRISH = 'ga';
    const LANGUAGE_ITALIAN = 'it';
    const LANGUAGE_JAPANESE = 'ja';
    const LANGUAGE_JAVANESE = 'jv';
    const LANGUAGE_KALAALLISUT_GREENLANDIC = 'kl';
    const LANGUAGE_KANNADA = 'kn';
    const LANGUAGE_KANURI = 'kr';
    const LANGUAGE_KASHMIRI = 'ks';
    const LANGUAGE_KAZAKH = 'kk';
    const LANGUAGE_KIKUYU_GIKUYU = 'ki';
    const LANGUAGE_KINYARWANDA = 'rw';
    const LANGUAGE_KIRGHIZ_KYRGYZ = 'ky';
    const LANGUAGE_KOMI = 'kv';
    const LANGUAGE_KONGO = 'kg';
    const LANGUAGE_KOREAN = 'ko';
    const LANGUAGE_KUANYAMA_KWANYAMA = 'kj';
    const LANGUAGE_KURDISH = 'ku';
    const LANGUAGE_LAO = 'lo';
    const LANGUAGE_LATIN = 'la';
    const LANGUAGE_LATVIAN = 'lv';
    const LANGUAGE_LIMBURGAN_LIMBURGER_LIMBURGISH = 'li';
    const LANGUAGE_LINGALA = 'ln';
    const LANGUAGE_LITHUANIAN = 'lt';
    const LANGUAGE_LUBA_KATANGA = 'lu';
    const LANGUAGE_LUXEMBOURGISH_LETZEBURGESCH = 'lb';
    const LANGUAGE_MACEDONIAN = 'mk';
    const LANGUAGE_MALAGASY = 'mg';
    const LANGUAGE_MALAY = 'ms';
    const LANGUAGE_MALAYALAM = 'ml';
    const LANGUAGE_MALTESE = 'mt';
    const LANGUAGE_MANX = 'gv';
    const LANGUAGE_MAORI = 'mi';
    const LANGUAGE_MARATHI = 'mr';
    const LANGUAGE_MARSHALLESE = 'mh';
    const LANGUAGE_MONGOLIAN = 'mn';
    const LANGUAGE_MONTENEGRIN = 'cnr';
    const LANGUAGE_NAURU = 'na';
    const LANGUAGE_NAVAJO_NAVAHO = 'nv';
    const LANGUAGE_NDONGA = 'ng';
    const LANGUAGE_NEPALI = 'ne';
    const LANGUAGE_NORTH_NDEBELE = 'nd';
    const LANGUAGE_NORTHERN_SAMI = 'se';
    const LANGUAGE_NORWEGIAN = 'no';
    const LANGUAGE_NORWEGIAN_BOKMÅL = 'nb';
    const LANGUAGE_NORWEGIAN_NYNORSK = 'nn';
    const LANGUAGE_OCCITAN = 'oc';
    const LANGUAGE_OJIBWA = 'oj';
    const LANGUAGE_ORIYA = 'or';
    const LANGUAGE_OROMO = 'om';
    const LANGUAGE_OSSETIAN_OSSETIC = 'os';
    const LANGUAGE_PALI = 'pi';
    const LANGUAGE_PASHTO_PUSHTO = 'ps';
    const LANGUAGE_PERSIAN = 'fa';
    const LANGUAGE_POLISH = 'pl';
    const LANGUAGE_PORTUGUESE = 'pt';
    const LANGUAGE_PORTUGUESE_BRAZIL = 'pt-br';
    const LANGUAGE_PUNJABI_PANJABI = 'pa';
    const LANGUAGE_QUECHUA = 'qu';
    const LANGUAGE_ROMANI = 'rom';
    const LANGUAGE_ROMANIAN_MOLDAVIAN = 'ro';
    const LANGUAGE_ROMANSH = 'rm';
    const LANGUAGE_RUNDI = 'rn';
    const LANGUAGE_RUSSIAN = 'ru';
    const LANGUAGE_SAMOAN = 'sm';
    const LANGUAGE_SANGO = 'sg';
    const LANGUAGE_SANSKRIT = 'sa';
    const LANGUAGE_SARDINIAN = 'sc';
    const LANGUAGE_SERBIAN = 'sr';
    const LANGUAGE_SERBO_CROATIAN = 'sh';
    const LANGUAGE_SHONA = 'sn';
    const LANGUAGE_SICHUAN_YI_NUOSU = 'ii';
    const LANGUAGE_SIGN_LANGUAGE = 'sign';
    const LANGUAGE_SIMPLIFIED_CHINESE = 'zhs';
    const LANGUAGE_SINDHI = 'sd';
    const LANGUAGE_SINHALA_SINHALESE = 'si';
    const LANGUAGE_SLOVAK = 'sk';
    const LANGUAGE_SLOVENIAN = 'sl';
    const LANGUAGE_SOMALI = 'so';
    const LANGUAGE_SOUTH_NDEBELE = 'nr';
    const LANGUAGE_SOUTHERN_SOTHO = 'st';
    const LANGUAGE_SPANISH_LATIN = 'es-LA';
    const LANGUAGE_SPANISH_CASTILIAN = 'es';
    const LANGUAGE_SUNDANESE = 'su';
    const LANGUAGE_SWAHILI = 'sw';
    const LANGUAGE_SWATI = 'ss';
    const LANGUAGE_SWEDISH = 'sv';
    const LANGUAGE_TAGALOG = 'tl';
    const LANGUAGE_TAHITIAN = 'ty';
    const LANGUAGE_TAJIK = 'tg';
    const LANGUAGE_TAMIL = 'ta';
    const LANGUAGE_TATAR = 'tt';
    const LANGUAGE_TELUGU = 'te';
    const LANGUAGE_THAI = 'th';
    const LANGUAGE_TIBETAN = 'bo';
    const LANGUAGE_TIGRINYA = 'ti';
    const LANGUAGE_TONGA_ISLANDS = 'to';
    const LANGUAGE_TRADITIONAL_CHINESE = 'zht';
    const LANGUAGE_TSONGA = 'ts';
    const LANGUAGE_TSWANA = 'tn';
    const LANGUAGE_TURKISH = 'tr';
    const LANGUAGE_TURKMEN = 'tk';
    const LANGUAGE_TWI = 'tw';
    const LANGUAGE_UIGHUR = 'ug';
    const LANGUAGE_UKRAINIAN = 'uk';
    const LANGUAGE_URDU = 'ur';
    const LANGUAGE_UZBEK = 'uz';
    const LANGUAGE_VENDA = 've';
    const LANGUAGE_VIETNAMESE = 'vi';
    const LANGUAGE_VOLAPÜK = 'vo';
    const LANGUAGE_WALLOON = 'wa';
    const LANGUAGE_WELSH = 'cy';
    const LANGUAGE_WESTERN_FRISIAN = 'fy';
    const LANGUAGE_WOLOF = 'wo';
    const LANGUAGE_XHOSA = 'xh';
    const LANGUAGE_YIDDISH = 'yi';
    const LANGUAGE_YORUBA = 'yo';
    const LANGUAGE_ZHUANG_CHUANG = 'za';
    const LANGUAGE_ZULU = 'zu';

    const TYPE_TEXT = 'txt';
    const TYPE_JSON = 'json';
    const TYPE_YAML = 'yaml';
    const TYPE_XML = 'xml';
    const TYPE_HTML = 'html';

    const WEBHOOK_TYPE_ALL = '*';

    const URL_DOWNLOAD = 'https://localise.translata.sk/api/translated/';
    const URL_TRANSLATE = 'https://localise.translata.sk/api/translate';
    const URL_TOKEN = 'https://localise.translata.sk/oauth/token';
    const URL_WEBHOOK_CREATE = 'https://localise.translata.sk/api/webhook';
    const URL_WEBHOOK_DELETE = 'https://localise.translata.sk/api/webhook/';

    const CLIENT_ID = 2;
    const CLIENT_SECRET = 'n0Lea9cYyNcRHYStaODf1rmdzI4wWFJTCGXz3rnp';

    const URL_TEST_DOWNLOAD = 'https://test-localise.translata.sk/api/translated/';
    const URL_TEST_TRANSLATE = 'https://test-localise.translata.sk/api/translate';
    const URL_TEST_TOKEN = 'https://test-localise.translata.sk/oauth/token';
    const URL_TEST_WEBHOOK_CREATE = 'https://test-localise.translata.sk/api/webhook';
    const URL_TEST_WEBHOOK_DELETE = 'https://test-localise.translata.sk/api/webhook/';

    const TEST_CLIENT_SECRET = 'ihTofiRDKjQfFiiU3VFHKlZuqZQB3fIrQLcekjDM';

    protected $token;
    protected $apiVersion = 1;

//    parameters
    protected $reference;
    protected $languages_to;
    protected $language_from;
    protected $jobDue;
    protected $preTranslate;
    protected $textType;

    /**
     *
     * @param string $token
     */

    public function __construct($token)
    {
        if(is_array($token))
        {
            $token = (object) $token;
        }

        $this->token = is_object($token) && property_exists($token, 'token')
            ?  $token->token
            : $token ;
    }

    /**
     * @param string $filePath
     * @return json
     * @throws \Exception
     */
    public function translateFile($filePath)
    {
        if(!file_exists($filePath))
        {
            throw new \Exception('File ' . $filePath . ' does not exist.');
        }

        return $this->translate($filePath, 'file');
    }

    public function translateUrl($url)
    {
        return $this->translate($url, 'url');
    }

    /**
     * @param string $text
     * @param string $type
     * @return json
     * @throws \Exception
     */

    public function translateText($text, $type=null)
    {
        if($type)
        {
            $this->withTextType($type);
        }

        if(!is_string($text) || !$text)
        {
            throw new \Exception('Missing text for translation.');
        }

        return $this->translate($text);
    }

    /**
     * @param string $value
     * @param string $type
     * @return mixed
     * @throws \Exception
     */

    private function translate($value, $type = 'text')
    {
        ini_set('display_errors', 1);

        $curl = static::initCurl($this->getTranslateUrl());

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept:application/json',
            'Authorization: Bearer '.$this->token,
            'Version:'. $this->apiVersion
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_POST, true);

        $options = [
            'sourceLan_code' => $this->language_from,
            'due_date'  => $this->jobDue ? $this->jobDue->format('Y-m-d H:i:s') : null,
            'reference' => $this->reference,
            'pre_translate' => $this->preTranslate
        ];

        $targetLangs = is_array($this->languages_to) ? $this->languages_to : [$this->languages_to];

        for ($i = 0; $i < count($targetLangs); $i++)
        {
            $options["targetLan_codes[$i]"] = $targetLangs[$i];
        }

        switch ($type)
        {
            case 'file':
                if (defined('PHP_MAJOR_VERSION') && PHP_MAJOR_VERSION > 5.5)
                {
                    $file = new \CURLFile($value);
                }
                else
                {
                    $file = '@' . realpath($value);
                }

                if(filesize($value) == 0)
                {
                    throw new \Exception('The selected file is empty!');
                }

                $options['file'] = $file;

                break;

            case 'text':
                $options['text'] = $value;

                if($this->textType)
                {
                    $options['content'] = $this->textType;
                }

                break;

            case 'url':
                $options['url'] = $value;

                break;

            default:
                throw new \Exception('Invalid translation type specified.');
        }

        $this->validateOptions($options);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $options);

        //$response = curl_exec($curl);
        $response = static::execCurl($curl);

        $this->checkResponseStatus($curl, $response);

        curl_close($curl);

        $this->clearOptions();

        $data = json_decode($response, true);

        return $data;
    }

    /**
     *
     * @param string $webhookName
     * @param string $webhookUrl
     * @param string $webhookType
     * @return json
     */

    public function createWebhook($webhookName, $webhookUrl, $webhookType = null)
    {
        $curl = static::initCurl($this->getWebhookCreateUrl());

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Accept:application/json',
                'Authorization: Bearer '.$this->token,
                'Version:'. $this->apiVersion
            ));
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

        $body = [
            'name' => $webhookName,
            'url' => $webhookUrl,
            'type' => $webhookType,
            ];

        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($body));

        //$response = curl_exec($curl);
        $response = static::execCurl($curl);

        $this->checkResponseStatus($curl, $response);

        curl_close($curl);

        $data = json_decode($response, true);

        return $data;
    }

    /**
     *
     * @param string $webhookNameOrUrl
     * @return json
     */

    public function deleteWebhook($webhookNameOrUrl)
    {
        $curl = static::initCurl($this->getWebhookDeleteUrl() . $webhookNameOrUrl);

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Accept:application/json',
                'Authorization: Bearer '.$this->token,
                'Version:'. $this->apiVersion
            ));
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);

        //$response = curl_exec($curl);
        $response = static::execCurl($curl);

        $this->checkResponseStatus($curl, $response);

        curl_close($curl);

        $data = json_decode($response, true);

        return $data;
    }

    /**
     * @param integer $jobId
     * @param string|null $path
     * @return array
     */
    public function downloadJob($jobId, $path = null, $pretranslated = false)
    {
        return $this->downloadJobFile($jobId, null, $path, $pretranslated);
    }

    /**
     * @param $reference
     * @param $targetLanguage
     * @param null $path
     * @return array
     */
    public function downloadJobByReference($reference, $targetLanguage, $path = null, $pretranslated = false)
    {
        return $this->downloadJobFile($reference, $targetLanguage, $path, $pretranslated);
    }

    /**
     * @param integer|string $jobIdOrReference
     * @param string|null $targetLanguage
     * @param string|null $path
     * @return array
     */
    private function downloadJobFile($jobIdOrReference, $targetLanguage = null, $path = null, $pretranslated = false)
    {
        $curl = curl_init();
        $downloadUrl = $this->getDownloadUrl($pretranslated) . $jobIdOrReference .  ($targetLanguage ? '/' . $targetLanguage : '');

        curl_setopt($curl, CURLOPT_URL, $downloadUrl);
        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Authorization: Bearer '.$this->token,
            'Version:'. $this->apiVersion
        ));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_POSTREDIR, 3);

        //$response = curl_exec($curl);
        $response = static::execCurl($curl);

        if($this->checkResponseStatus($curl, $response) == 404)
        {
            throw new \Exception('Translation not found.');
        }

        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);
        $data = json_decode($body);

        if($path)
        {
            file_put_contents(rtrim($path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . ltrim($data->filename, DIRECTORY_SEPARATOR), $data->file);
        }

        return (array)$data;
    }

    /**
    *
     * @param string $languageFrom
     * @return $this
     * @deprecated since 1.04 use withLanguageFrom instead
     */
    public function withLanaguageFrom($languageFrom)
    {
        $this->language_from = $languageFrom;

        return $this;
    }

    /**
    *
     * @param string $languageFrom
     * @return $this
     */
    public function withLanguageFrom($languageFrom)
    {
        $this->language_from = $languageFrom;

        return $this;
    }

    /**
     *
     * @param string|array $languagesTo
     * @return $this
     */

    public function withLanguagesTo($languagesTo)
    {
        $this->languages_to = $languagesTo;

        return $this;
    }

    /**
     *
     * @param string $reference max 255 characters
     * @return $this
     */

    public function withReference($reference)
    {
        if(strlen($reference) > 255)
        {
            throw new \Exception('Max size of reference is 255 characters.');
        }

        $this->reference = $reference;

        return $this;
    }

    /**
     *
     * @param DateTime $jobDue
     * @return $this
     */

    public function withJobDue($jobDue)
    {
        if(!$jobDue instanceof DateTime)
        {
            throw new \Exception('Date time instance must be given for job due');
        }

        $this->jobDue = $jobDue;

        return $this;
    }

    /**
     * Pretranslation is off by default. You can activate it with this method.
     *
     * @return $this
     */

    public function withPreTranslate()
    {
        $this->preTranslate = 1;

        return $this;
    }


    /**
     * $textType should match const TYPE_... of this class
     *
     * @param string $textType
     * @return $this
     */

    public function withTextType($textType)
    {
        $this->textType = $textType;

        return $this;
    }

    /**
     * Unset options to avoid mixing options form different requests.
     */

    private function clearOptions()
    {
        unset($this->reference);
        unset($this->languages_to);
        unset($this->language_from);
        unset($this->jobDue);
        unset($this->preTranslate);
        unset($this->textType);
    }

    private function validateOptions(&$options)
    {
        if(!isset($options['targetLan_codes[0]']))
        {
            throw new \Exception('Target languages not specified. Use withLanguagesTo() method to set target languages');
        }

        if(!isset($options['sourceLan_code']))
        {
            throw new \Exception('Source language not specified. Use withLanguagesFrom() method to set source language');
        }
    }

    /**
     * Creates new token. If old token is given and is valid returns it.
     * If token is no longer valid uses its refresh token to obtain new one.
     * If refresh token is invalid or not given uses name and password to obtain new one.
     *
     * Token should be persisted and used again in order to speed up the process.
     * Method returns new token in $token->token != $newToken->token
     *
     * @param string $userName
     * @param string $password
     * @param object $token
     * @return type
     */

    public static function getToken($userName, $password, $token = null)
    {
        if(is_object($token) && new DateTime($token->until) > new DateTime())
        {
            //original token is still valid
            return $token;
        }

        $data = 'grant_type=password&username=' . $userName . '&password=' . $password . '&client_id=' . static::CLIENT_ID . '&client_secret=' . static::getClientSecret();
        $curl = static::initCurl(static::getTokenUrl());

        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($curl, CURLOPT_HEADER, FALSE);
        //curl_setopt($curl, CURLOPT_HEADER, TRUE);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        //$response = curl_exec($curl);
        $response = static::execCurl($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if($code != 200)
        {
            if($code == 401)
            {
                throw new \Exception('Invalid credentials.');
            }

            throw new \Exception('Authentification failed.');
        }

        $authData = json_decode($response, true);
        $seconds = $authData['expires_in'] - 300; //cut 5 minutes of token lifetime

        return (object) [
            'token' => $authData['access_token'],
            'until' => (new DateTime())->modify("+$seconds seconds")->format('Y-m-d H:i:s'),
            'refresh' => $authData['refresh_token']
        ];
    }

    private function checkResponseStatus($curl, $response)
    {
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if($code != 200)
        {
            switch ($code)
            {
                case 401:
                    throw new \Exception('Invalid credentials.');

                case 500:
                    $this->throwServerError($response);
            }
        }

        return $code;
    }

    private function throwServerError($response)
    {
        $data = json_decode($response);

        if(is_object($data) && (isset($data->error) || isset($data->errors)))
        {
            if(isset($data->error))
            {
                throw new \Exception($data->error);
            }

            if(is_array($data->errors))
            {
                $message = implode("\n", $data->errors);

                throw new \Exception($message);
            }
        }

        throw new \Exception('Server error contact support.');
    }

    public function getDownloadUrl($pretranslated=false)
    {
        return (static::isInTestMode() ? static::URL_TEST_DOWNLOAD : static::URL_DOWNLOAD)
            . ($pretranslated ? '?pretranslated=1' : '');
    }

    public function getTranslateUrl()
    {
        return static::isInTestMode() ? static::URL_TEST_TRANSLATE : static::URL_TRANSLATE;
    }

    public static function getTokenUrl()
    {
        return static::isInTestMode() ? static::URL_TEST_TOKEN : static::URL_TOKEN;
    }

    public function getWebhookCreateUrl()
    {
        return static::isInTestMode() ? static::URL_TEST_WEBHOOK_CREATE : static::URL_WEBHOOK_CREATE;
    }

    public function getWebhookDeleteUrl()
    {
        return static::isInTestMode() ? static::URL_TEST_WEBHOOK_DELETE : static::URL_WEBHOOK_DELETE;
    }

    public static function getClientSecret()
    {
        return static::isInTestMode() ? static::TEST_CLIENT_SECRET : static::CLIENT_SECRET;
    }

    private static function isInTestMode()
    {
        return getenv('LOCALISE_TEST') === "true";
    }

    private static function initCurl($url)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

        return $curl;
    }

    private static function execCurl($curl)
    {
        $headers = [];

        //get headers form curl
        curl_setopt($curl, CURLOPT_HEADERFUNCTION,function($curl, $header) use (&$headers){
            $len = strlen($header);
            $header = explode(':', $header, 2);

            if (count($header) < 2) // ignore invalid headers
            {
                return $len;
            }

            $headers[strtolower(trim($header[0]))][] = trim($header[1]);

            return $len;
        });

        //retry if too many attempts is returned
        while(true)
        {
            $response = curl_exec($curl);

            // 503 service unavailable
            if(curl_getinfo($curl, CURLINFO_HTTP_CODE) == 503)
            {
                throw new \Exception('Server maintenance in progress. Please try again in a few moments.');
            }

            //429 too many requests
            if(curl_getinfo($curl, CURLINFO_HTTP_CODE) == 429 && isset($headers['retry-after']))
            {
                sleep($headers['retry-after']);
                continue;
            }

            break ;
        }

        return $response;
    }
}
